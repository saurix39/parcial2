<?php
require "fpdf/fpdf.php";
require "Logica/Tienda.php";
$tienda=new Tienda();
$tiendas=$tienda -> consultarTiendas($_POST["producto"]);
$tabla =new FPDF("P","mm","Letter");
$tabla -> SetFont("Courier", "B", 20);
$tabla -> AddPage();
$tabla ->SetXY(0, 0);
$tabla -> Cell(216, 20, "Parcial 2", 0, 2, "C");
$tabla -> Cell(216, 15, "Reporte Productos", 0, 1, "C");
$tabla -> SetFont("Courier", "B", 17);
$tabla -> Cell(65,10,"Nombre",1,0,"C");
$tabla -> Cell(65,10,"Direccion",1,0,"C");
$tabla -> Cell(65,10,"Cantidad",1,1,"C");
$tabla -> SetFont("Courier", "B", 15);
foreach($tiendas as $tiend){
        $tabla -> Cell(65,20,$tiend -> getTienda() -> getNombre() ,1,0,"C");
        $tabla -> Cell(65,20,$tiend -> getTienda() -> getDireccion() ,1,0,"C");
        $tabla -> Cell(65,20,$tiend -> getCantidad(),1,1,"C");
}
$tabla -> Output();
?>