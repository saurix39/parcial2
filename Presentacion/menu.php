<div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="index.php">Inicio</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Producto
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Producto/insertarProducto.php") ?>" >Insertar</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Producto/consultarProductos.php") ?>">Consultar</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Producto/unidades.php") ?>">Unidades</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Producto/inventarioProducto.php") ?>" target="_blank">Inventario Producto</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Tienda
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Tienda/insertarTienda.php") ?>">Insertar</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Tienda/consultarTiendas.php") ?>">Consultar</a>
                </div>
            </li>
            </ul>
        </div>
    </nav>
</div>
