<?php
$registrado=false;
$producto=new Producto();
$productos=$producto -> consultarTodos();
$tienda=new Tienda();
$tiendas=$tienda -> consultarTodos();
if(isset($_POST["registrar"])){
    $tiendaid=$_POST["tienda"];
    $productoid=$_POST["producto"];
    $cantidad=$_POST["cantidad"];
    $producto=new Producto($productoid);
    $unidades=$producto -> conUnid($tiendaid);
    $total=$unidades+$cantidad;
    if($unidades>0){
        $producto -> actualizarUnidades($tiendaid,$total);
    }else{
        $producto -> insertarUnidades($tiendaid,$cantidad);
    }
    $registrado=true;
    
}
?>
<div class="container">
    <div class="row">
        <div class="col-2">
        </div>
        <div class="col-md-8 col-12">
            <div class="card border border-dark mt-1">
                <div class="card-header text-center">
                    <h2>Agregar unidades de un producto</h2>
                </div>
                <div class="card-body">
                <form action="index.php?pid=<?php echo base64_encode("Presentacion/Producto/unidades.php")?>" method="post">
                        <label>Tienda</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Opciones</label>
                            </div>
                            <select class="custom-select" name="tienda" required>
                                <option selected>Escoger...</option>
                                <?php
                                foreach($tiendas as $tien){
                                    echo "<option value='". $tien -> getIdTienda() ."'>". $tien -> getNombre() ."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <label>Producto</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Opciones</label>
                            </div>
                            <select class="custom-select" name="producto" required>
                                <option selected>Escoger...</option>
                                <?php
                                foreach($productos as $proc){
                                    echo "<option value='". $proc -> getIdProducto() ."'>". $proc -> getNombre() ."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Cantidad</label>
                            <input type="number" name="cantidad" min="1" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="registrar" class="form-control btn btn-secondary">Registrar unidades</button>
                        </div> 
                        <?php if($registrado){?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                Datos ingresados con exito!!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


                        