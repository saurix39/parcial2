<?php
$registrado = false;
?>
<div class="container">
    <div class="row">
        <div class="col-2">
        </div>
        <div class="col-md-8 col-12">
            <div class="card border border-dark mt-1">
                <div class="card-header text-center">
                    <h2>Inventario producto</h2>
                </div>
                <div class="card-body">
                <form action="pdfProducto.php" method="post">
                        <div class="form-group">
                            <label>Identificación del producto</label>
                            <input type="number" name="producto" min="1" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="buscar" class="form-control btn btn-secondary">Crear pdf</button>
                        </div> 
                        <?php if($registrado){?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                Datos ingresados con exito!!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>