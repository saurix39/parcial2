<?php
$tienda=new Tienda();
$tiendas=$tienda -> consultarTodos();
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card border border-dark mt-1">
                <div class="card-header text-center">
                    <h2>Consultar tiendas</h2>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">dirección</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($tiendas as $tien){
                                    echo "<tr>";
                                    echo "<td>". $tien -> getIdTienda()."</td>";
                                    echo "<td>". $tien -> getNombre()."</td>";
                                    echo "<td>". $tien -> getDireccion() ."</td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>