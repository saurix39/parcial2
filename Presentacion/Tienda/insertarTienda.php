<?php
$registrado=false;
$error=0;
if(isset($_POST["registrar"])){
    $nombre=$_POST["nombre"];
    $direccion=$_POST["direccion"];
    $tienda = new Tienda("", $nombre,$direccion);
    $tienda -> insertarTienda();
    $registrado=true;

}
?>
<div class="container">
    <div class="row">
        <div class="col-2">
        </div>
        <div class="col-md-8 col-12">
            <div class="card border border-dark mt-1">
                <div class="card-header text-center">
                    <h2>Registrar producto</h2>
                </div>
                <div class="card-body">
                <form action="index.php?pid=<?php echo base64_encode("Presentacion/Tienda/insertarTienda.php")?>" method="post">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" name="nombre" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Direccion</label>
                            <input type="text" name="direccion" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="registrar" class="form-control btn btn-secondary">Registrar tienda</button>
                        </div> 
                        <?php if($registrado){?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                Datos ingresados con exito!!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>