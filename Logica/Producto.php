<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/ProductoDAO.php";
class Producto{
    private $idProducto;
    private $nombre;
    private $precio;
    private $productoDAO;
    private $conexion;

    public function Producto($idProducto="",$nombre="",$precio=""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> productoDAO = new ProductoDAO($idProducto,$nombre,$precio);
        $this -> conexion = new Conexion();

    }

    public function insertarProducto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> insertarProducto());
        $this -> conexion -> cerrar();
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();        
        return $productos;
    }

    public function insertarUnidades($tiendaid,$cantidad){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> insertarUnidades($tiendaid,$cantidad));
        $this -> conexion -> cerrar();
    }

    public function conUnid($tiendaid){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> conUnid($tiendaid));
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas()==1){
            $resultado=$this -> conexion -> extraer(); 
            return $resultado[0];
        }else{
            return 0;
        }
    }

    public function actualizarUnidades($tiendaid,$cantidad){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> actualizarUnidades($tiendaid,$cantidad));
        $this -> conexion -> cerrar();
    }
 
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getPrecio()
    {
        return $this->precio;
    }
}
?>