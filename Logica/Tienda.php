<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/TiendaDAO.php";
require_once "Logica/Producto_Tienda.php";
class Tienda{
    private $idTienda;
    private $nombre;
    private $direccion;
    private $tiendaDAO;
    private $conexion;

    public function Tienda($idTienda="",$nombre="",$direccion=""){
        $this -> idTienda=$idTienda;
        $this -> nombre=$nombre;
        $this -> direccion=$direccion;
        $this -> tiendaDAO=new TiendaDAO($idTienda,$nombre,$direccion);
        $this -> conexion=new Conexion();
    }

    public function insertarTienda(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tiendaDAO -> insertarTienda());
        $this -> conexion -> cerrar();
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tiendaDAO -> consultarTodos());
        $tiendas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $t = new Tienda($resultado[0], $resultado[1], $resultado[2]);
            array_push($tiendas, $t);
        }
        $this -> conexion -> cerrar();        
        return $tiendas;
    }

    public function consultarTiendas($producto){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tiendaDAO -> consultarTiendas($producto));
        $tiendas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $t = new Tienda($resultado[0], $resultado[1], $resultado[2]);
            $p= new Producto_Tienda("",$t,$resultado[3]);
            array_push($tiendas, $p);
        }
        $this -> conexion -> cerrar();        
        return $tiendas;
    }

    public function getIdTienda()
    {
        return $this->idTienda;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getDireccion()
    {
        return $this->direccion;
    }
}
?>