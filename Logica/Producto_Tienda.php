<?php
class Producto_Tienda{
    private $producto;
    private $tienda;
    private $cantidad;
    public function Producto_Tienda($producto="",$tienda="",$cantidad=""){
        $this -> producto=$producto;
        $this -> tienda=$tienda;
        $this -> cantidad=$cantidad;

    }

    public function getProducto()
    {
        return $this->producto;
    }

    public function getTienda()
    {
        return $this->tienda;
    }

    public function getCantidad()
    {
        return $this->cantidad;
    }
}
?>