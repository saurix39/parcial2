<?php
class TiendaDAO{
    private $idTienda;
    private $nombre;
    private $direccion;

    public function TiendaDAO($idTienda="",$nombre="",$direccion=""){
        $this -> idTienda=$idTienda;
        $this -> nombre=$nombre;
        $this -> direccion=$direccion;
    }

    public function insertarTienda(){
        return "insert into tienda (nombre,direccion) values('". $this -> nombre ."', '". $this -> direccion ."')";
    }

    public function consultarTodos(){
        return "select id_tienda, nombre, direccion from tienda";
    }

    public function consultarTiendas($producto){
        return "select id_tienda, nombre, direccion, cantidad from tienda inner join producto_tienda on (id_tienda=id_tienda_fk) where id_producto_fk='".$producto."'";
    }

}
?>