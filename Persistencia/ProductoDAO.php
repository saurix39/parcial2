<?php
class ProductoDAO{
    private $idProducto;
    private $nombre;
    private $precio;
    
    public function ProductoDAO($idProducto="",$nombre="",$precio=""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
    }

    public function insertarProducto(){
        return "insert into producto (nombre,precio) values('". $this -> nombre ."', '". $this -> precio."')";
    }

    public function consultarTodos(){
        return "select id_producto, nombre, precio from producto";
    }

    public function conUnid($tiendaid){
        return "select cantidad from producto_tienda where id_producto_fk='". $this -> idProducto  ."' and id_tienda_fk='". $tiendaid ."'";
    }

    public function insertarUnidades($tiendaid,$cantidad){
        return "insert into producto_tienda (id_producto_fk,id_tienda_fk,cantidad) values('". $this -> idProducto ."', '". $tiendaid ."', '". $cantidad ."')";
    }

    public function actualizarUnidades($tiendaid,$cantidad){
        return "update producto_tienda set cantidad='". $cantidad ."' where id_producto_fk='". $this -> idProducto ."' and id_tienda_fk='". $tiendaid ."'";
    }
 
}
?>